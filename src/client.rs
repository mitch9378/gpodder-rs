use http::status::StatusCode;
use log::debug;
use reqwest::{Client, Url};
mod device;
use device::{Device, DeviceType, DeviceUpdates};
mod error;
use error::GpodError;

/**
 * base_url: base URL for gpodder.net or compatible server
 * username: server user name
 * password: user password
 * client: reqwest client
 */
pub struct GpodClient {
    pub base_url: String,
    pub username: String,
    password: String,
    client: Client,
}

///Client for gpodder.net or compatible server
impl GpodClient {
    ///Returns a new instance of the GpodClient
    pub fn new() -> Result<GpodClient, GpodError> {
        let client = Client::builder().cookie_store(true).build();

        let client = match client {
            Ok(client) => client,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    "Couldn't build http client bad config".to_string(),
                ))
            }
        };

        return Ok(GpodClient {
            base_url: "https://gpodder.net".to_string(),
            username: "".to_string(),
            password: "".to_string(),
            client: client,
        });
    }

    ///Set the password for authentication
    pub fn set_password(&mut self, pass: &String) {
        self.password = pass.to_string();
    }

    /**
     * Returns the status code of the authentication request
     *
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/auth.html#login-verify-login
     */
    pub async fn authenticate(&self) -> Result<StatusCode, GpodError> {
        debug!("Logging in as: {}", &self.username);

        let url_str = [
            &self.base_url,
            "/api/2/auth/",
            &self.username,
            "/login.json",
        ]
        .join("");
        let url = Url::parse(&url_str);
        let url = match url {
            Ok(url) => url,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to parse URL: ".to_string(), url_str].join(""),
                ))
            }
        };

        let result = self
            .client
            .post(url)
            .body("")
            .basic_auth(&self.username, Some(&self.password))
            .send()
            .await;

        let result = match result {
            Ok(result) => result,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to post to authentication: ".to_string(), url_str].join(""),
                ))
            }
        };

        let status_code = result.status();

        debug!("POST Authenticate HTTP status: {}", status_code);

        Ok(status_code)
    }

    /**
     * Updates the caption and device type for the given device_id
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#update-device-data
     */
    pub async fn update_device_data(
        &self,
        device_id: &String,
        caption: &String,
        device_type: DeviceType,
    ) -> Result<StatusCode, GpodError> {
        device::Device::update_device_data(&self, device_id, caption, device_type).await
    }

    /**
     * Lists all the gpodder.net devices belonging to the user
     *
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#list-devices
     */
    pub async fn list_devices(&self) -> Result<Vec<Device>, GpodError> {
        device::Device::list_devices(&self).await
    }

    /**
     * Returns all the updates available for the provided device
     *
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
     *
     * since: seconds since EPOCH time
     */
    pub async fn device_updates(
        &self,
        device_id: &String,
        since: i64,
        include_actions: bool,
    ) -> Result<DeviceUpdates, GpodError> {
        device::Device::device_updates(&self, device_id, since, include_actions).await
    }
}
