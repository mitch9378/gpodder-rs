use crate::client::error::GpodError;
use crate::client::GpodClient;
use http::status::StatusCode;
use log::debug;
use std::collections::HashMap;
use url::Url;

/**
 * Contains information about the the device
 * id: Unique identifer for the device
 * caption: Description or further information about the device
 * type: The devices type as defined in gpodder or compatible server
 */
#[derive(Deserialize, Debug)]
pub struct Device {
    id: String,
    caption: String,
    r#type: String,
    subscriptions: i32,
}

/**
 * Valid device types
 * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#update-device-data
 * */
#[derive(Debug, Deserialize)]
pub enum DeviceType {
    Desktop,
    Laptop,
    Mobile,
    Server,
    Other,
}

impl DeviceType {
    /**
     * Prints the DeviceType as a lowercase str
     */
    pub fn to_str(&self) -> &'static str {
        match self {
            DeviceType::Desktop => "desktop",
            DeviceType::Laptop => "laptop",
            DeviceType::Mobile => "mobile",
            DeviceType::Server => "server",
            DeviceType::Other => "other",
        }
    }
}

/**
 * Valid statuses for device updates
 * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
 */
#[derive(Debug, Deserialize)]
pub enum Status {
    New,
    Play,
    Download,
    Delete,
}

// impl Status{
/**
 * Returns the status as a lowercase str
 */
//     pub fn to_str(&gpod) -> &'static str {
//         match *gpod {
//             Status::New => "new",
//             Status::Play => "play",
//             Status::Download => "download",
//             Status::Delete => "delete",
//         }
//     }
// }

/**
 * a list of subscriptions to be added, with URL, title and descriptions
 * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
 */
#[derive(Deserialize, Debug)]
pub struct Add {
    title: String,
    url: Url,
    description: String,
    subscribers: i32,
    logo_url: Url,
    website: Url,
    mygpo_link: Url,
}

/**
 * a list of updated episodes
 * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
 */
#[derive(Deserialize, Debug)]
pub struct Updates {
    title: String,
    url: Url,
    podcast_title: String,
    podcast_url: Url,
    description: String,
    website: Url,
    mygpo_link: Url,
    released: String,
    status: Status,
}

/**
 * Represents the updates available for the device
 * add: a list of subscriptions to be added, with URL, title and descriptions
 * remove: a list of URLs to be unsubscribed
 * updates: a list of updated episodes
 * timestamp: the current timestamp; for retrieving changes since the last query, duration in
 * seconds from EPOCH time
 * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
 */
#[derive(Deserialize, Debug)]
pub struct DeviceUpdates {
    add: Vec<Add>,
    remove: Vec<Url>,
    updates: Vec<Updates>,
    timestamp: i64,
}

impl Device {
    /**
     * Updates the caption and device type for the given device_id
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#update-device-data
     */
    pub async fn update_device_data(
        gpod: &GpodClient,
        device_id: &String,
        caption: &String,
        device_type: DeviceType,
    ) -> Result<StatusCode, GpodError> {
        let url_str = [
            &gpod.base_url,
            "/api/2/devices/",
            &gpod.username,
            "/",
            &device_id,
            ".json",
        ]
        .join("");
        let url = Url::parse(&url_str);
        let url = match url {
            Ok(url) => url,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to parse URL: ".to_string(), url_str].join(""),
                ))
            }
        };

        let mut body = HashMap::new();
        body.insert("caption", &caption);
        let device_type_string = &device_type.to_str().to_string();
        body.insert("type", &device_type_string);

        let result = gpod.client.post(url).json(&body).send().await;

        let result = match result {
            Ok(result) => result,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    [
                        "Failed to post to update device data: ".to_string(),
                        url_str,
                    ]
                    .join(""),
                ))
            }
        };

        let status_code = result.status();
        debug!("POST Update Device data HTTP status: {}", result.status());

        Ok(status_code)
    }

    /**
     * Lists all the gpodder.net devices belonging to the user
     *
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#list-devices
     */
    pub async fn list_devices(gpod: &GpodClient) -> Result<Vec<Device>, GpodError> {
        let url_str = [&gpod.base_url, "/api/2/devices/", &gpod.username, ".json"].join("");
        let url = Url::parse(&url_str);
        let url = match url {
            Ok(url) => url,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to parse URL: ".to_string(), url_str].join(""),
                ))
            }
        };

        let result = gpod.client.get(url).send().await;

        let result = match result {
            Ok(result) => result,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to GET devices: ".to_string(), url_str].join(""),
                ))
            }
        };

        debug!("GET list devices HTTP status: {}", result.status());

        if result.status() != StatusCode::OK {
            return Err(GpodError::new(
                None,
                [
                    url_str,
                    " List devices failed with HTTP status: ".to_string(),
                    result.status().to_string(),
                ]
                .join(""),
            ));
        }

        let device_list = result.json::<Vec<Device>>().await;

        let device_list = match device_list {
            Ok(device_list) => device_list,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    "Failed to serialize device list".to_string(),
                ))
            }
        };

        Ok(device_list)
    }

    /**
     * Returns all the updates available for the provided device
     *
     * https://gpoddernet.readthedocs.io/en/latest/api/reference/devices.html#get-device-updates
     *
     * since: seconds since EPOCH time
     */
    pub async fn device_updates(
        gpod: &GpodClient,
        device_id: &String,
        since: i64,
        include_actions: bool,
    ) -> Result<DeviceUpdates, GpodError> {
        let url_str = [
            &gpod.base_url,
            "/api/2/updates/",
            &gpod.username,
            "/",
            &device_id,
            ".json",
        ]
        .join("");

        let url = Url::parse(&url_str);
        let url = match url {
            Ok(url) => url,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to parse URL: ".to_string(), url_str].join(""),
                ))
            }
        };

        //let dur = since.signed_duration_since::<chrono::Utc>(chrono::DateTime::from(SystemTime::UNIX_EPOCH));
        let parameters = [
            ("since", since.to_string()),
            ("include_actions", include_actions.to_string()),
        ];

        debug!("query params: {:#?}", &parameters);

        let result = gpod.client.get(url).query(&parameters).send().await;

        let result = match result {
            Ok(result) => result,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    ["Failed to GET device updates: ".to_string(), url_str].join(""),
                ))
            }
        };

        debug!("GET device updates HTTP status: {}", result.status());

        if result.status() != StatusCode::OK {
            return Err(GpodError::new(
                None,
                [
                    url_str,
                    " device updates failed with HTTP status:".to_string(),
                    result.status().to_string(),
                ]
                .join(""),
            ));
        }

        let device_updates = result.json::<DeviceUpdates>().await;

        let device_updates = match device_updates {
            Ok(device_list) => device_list,
            Err(error) => {
                return Err(GpodError::new(
                    Some(Box::new(error)),
                    "Failed to serialize device updates".to_string(),
                ))
            }
        };

        Ok(device_updates)
    }
}
