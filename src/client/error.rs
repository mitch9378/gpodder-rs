use std::error::Error;
use std::fmt;

pub(crate) type BoxError = Box<dyn Error + Send + Sync>;

#[derive(Debug)]
pub struct GpodError {
    source: Option<BoxError>,
    error_body: String,
}

impl GpodError {
    pub fn new(source: Option<BoxError>, description: String) -> GpodError {
        GpodError {
            source: source,
            error_body: description,
        }
    }
}

impl fmt::Display for GpodError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.error_body)
    }
}

impl Error for GpodError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_ref().map(|e| &**e as _)
    }
}
