extern crate gpodder;
extern crate tokio;

use chrono::prelude::*;
use gpodder::client::GpodClient;
use std::time::SystemTime;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let mut gpod = GpodClient::new().unwrap();
    gpod.username = "mitch9378".to_string();
    gpod.set_password(&"".to_string());

    let status = gpod.authenticate().await.unwrap();
    println!("status code: {}", status);

    let devices = gpod.list_devices().await.unwrap();
    println!("devices: {:?}", &devices);

    let dt = Utc
        .ymd(2020, 05, 21)
        .and_hms(9, 10, 11)
        .signed_duration_since::<chrono::Utc>(chrono::DateTime::from(SystemTime::UNIX_EPOCH))
        .num_seconds();
    let device_updates = gpod
        .device_updates(&"".to_string(), dt, false)
        .await
        .unwrap();
    println!("device updates: {:?}", &device_updates);
}
